require("./css/style.css");

import 'ol/ol.css';
import Map from 'ol/map';
import View from 'ol/view';
import TileLayer from 'ol/layer/tile';
import XYZSource from 'ol/source/xyz';
import OSM from 'ol/source/osm';
import Stamen from 'ol/source/stamen';
import proj from 'ol/proj';
import VectorSource from 'ol/source/vector';
import VectorLayer from 'ol/layer/vector';
import GeoJSON from 'ol/format/geojson';
import Style from 'ol/style/style';
import Stroke from 'ol/style/stroke';
import Fill from 'ol/style/fill';
import Circle from 'ol/style/circle';
import Overlay from 'ol/overlay';
import TileJSON from 'ol/source/tilejson';
import coordinate from 'ol/coordinate';

import costdata from './data/costmembers.json';

/**
 * Handle the popup window in its own overlay
 */
let container = document.getElementById('map-info-box');
let content = document.getElementById('map-info-text');
let header = document.getElementById('map-info-header');

/**
 * Style the GeoJSON features 
 */
let styleFunction = function(feature) {
    return new Style({
        image: new Circle({
            radius: 6,
            fill: new Fill({color: 'rgba(200,100,100,0.7)'}),
            stroke: new Stroke({color: 'rgb(200,80,80)', width: 1})
        })
    });
}

/**
 * Create a layer from the GeoJSON features
 */
let vectorSource = new VectorSource({
    features: (new GeoJSON()).readFeatures(costdata, {
        featureProjection: 'EPSG:3857'
        // featureProjection: 'EPSG:4326'
        // also interesting: dataProjection
    })
});
let vectorLayer = new VectorLayer({
    source: vectorSource,
    style: styleFunction
});

/**
 * Create the actual map
 */
let map = new Map({
    target: 'map-container',
    layers: [
        new TileLayer({
            source: new TileJSON({
                url: 'https://api.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy.json?secure',
                crossOrigin: 'anonymous',
                attributions: 'Map made with <a href="http://www.naturalearthdata.com">Natural Earth</a>.'
            })
            // Some more map designs....
            // source: new Stamen({
            //     layer: "terrain-background"
            // })
            // source: new OSM()
        }),
        vectorLayer
    ],
    //overlays: [overlay],
    view: new View({
        center: proj.fromLonLat([13.0, 47.5]),
        zoom: 4
    })
});

/**
 * Add click handler to the map to handle the popup
 */
map.on('singleclick', function(event) {
    // Check if there is a feature at the click position
    let hit = map.hasFeatureAtPixel(event.pixel);
    if (hit) {
        let newHeader = '',
            newText = '';
        // Iterate over all features (should only be one.. but who knows ;))
        map.forEachFeatureAtPixel(event.pixel, function(feature) {
            // Each feature should have 'name', 'address' and 'description'
            // properties.
            let name = feature.get('name'),
                address = feature.get('address'),
                description = feature.get('description');
            if (newHeader === '') { newHeader = address; }
            newText += '<p><span class="name">'+name+'</span><br/><span class="desc">'+description+'</span></p>';
        });
        header.innerHTML = newHeader;
        content.innerHTML = newText;
    }
});

map.on('pointermove', function(event) {
    let hit = map.hasFeatureAtPixel(event.pixel);
    document.getElementById(map.getTarget()).style.cursor = hit ? 'pointer' : '';
});
