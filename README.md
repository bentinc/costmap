# costmap.js

Visualize members of the COST action group 'distant reading' on a map.


## Test the map

For testing it suffices to start a development server in the root directory, e.g. with python:

    python3 -m http.server

After that, the test page will be available under http://localhost:8000.


## Adjusting the data and/or the map

If you want to change something in the data to render, edit `data/COSTMembers.csv`
accordingly and then run `csv2geo.py` to auto-update the GeoJSON file `data/costmembers.json`
which serves as the actual data source for the map. E.g.

    ./csv2geo.py data/COSTMembers.csv data/costmembers.json
    
If you want to change the actual behavior of the map, edit `main.js` instead. In any case, webpack
needs to be rerun afterwards to bundle all files into one single file
`bundle.js` which then get included by `index.html`.

If all you want to do is changing some styles, you could edit `css/style.css` but you can also just
include some styling directly in the HTML. The latter has the benefit that webpack does not need
to run again.
