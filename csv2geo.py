#!/usr/bin/env python3
#
"""
Read CSV from geobrowser and generate a GeoJSON file.
"""

import csv
import json

# Labels of the columns
COLUMNS = [
    'Name',
    'Address',
    'Name',
    'Longitude',
    'Latitude',
    'Member Status',     
    'Working groups',
    'Country',
]


def read_rows(filepath):
    with open(filepath, newline='') as f:
        reader = csv.DictReader(f)
        return list(reader)


def get_description(row):
    desc = "{0}.<br>Working groups: {1}".format(
        row['Member Status'],
        row['Working groups'])
    return desc


def get_address(row):
    country, city = row['Country'], row['Address']
    if country == city:
        return "{}".format(country)
    return "{0} ({1})".format(city, country)


def row_to_object(row):
    """Given a row from the CSV, generate an object which is
    suitable as a geojson feature.
    """
    try:
        obj = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [ 
                    float(row['Longitude']),
                    float(row['Latitude'])
                ],
            },
            "properties": {
                "name": row['Name'],
                "address": get_address(row),
                "description": get_description(row), 
            },
        }
    except ValueError as e:
        # This is caused by either empty rows in the csv or by missing
        # coordinates. We can't render these entries anyway, so ignore them.
        return None
    return obj


def write_json(rows, filepath):
    geojson = {
        "type": "FeatureCollection",
        "features": []
    }
    objs = [row_to_object(row) for row in rows]
    objs = [o for o in objs if o is not None]
    geojson["features"] = objs
    with open(filepath, 'w') as jsonfile:
        json.dump(geojson, jsonfile, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 3:
        sys.exit("Usage: csv2geo <infile> <outfile>")

    rows = read_rows(sys.argv[1])
    write_json(rows, sys.argv[2])


